<?php

// present dans l'index du site ou doit etre sur toutes les pages.
session_start();

require_once 'config.php';
require_once 'lib/db.php';


include_once 'view/header.html';
include_once 'view/menu.php';
include_once 'view/content.php';
include_once 'view/footer.html';
