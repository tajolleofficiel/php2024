<?php
// le formulaire ne doit pas être vide
if (empty($_POST)) {
    header("HTTP/1.1 405");
    die;
}

// l'email doit être valide
$data = validDataType($_POST);
// connexion à la DB
$con = connect();
// requête SQL
$result = $con->prepare("SELECT * FROM user WHERE email = ?");
// si on veut remplacer '*' on mettra l'id et password
// Exécution de la requête avec le paramètre
$result->execute([$data['email']]);
// FETCH (transformation du résultat de la requête en objet PHP)
$user = $result->fetchObject();
// si il y avait juste un fetch il sortira un tableau associatif
// si l'utilisateur n'existe pas, une erreur est renvoyée
if (!is_object($user)) {
    header("HTTP/1.1 401");
    die;
}
// comparaison entre le mot de passe du formulaire et le mot de passe crypté en DB

if (password_verify($data['password'], $user->password)) {
    // uptade user->updat
    // on a besoin $value et $id(userid)
    // 1. verrification si on est connecter a la DB
    // 2. query
    // execute ([..., $user->id]) les deux parametres.

    $update = $con->prepare("UPDATE user SET updated = NOW() WHERE id = ?");
    $update->execute([$user->id]);
    if($update->rowCount()) {
        echo "profil mis a jours";
    } else{
        echo "profil non mis a jours";
    }

    $_SESSION['userid'] = $user->id;
    $_SESSION['message'] = 'Vous êtes connecté';
    header('Location: index.php?slug=view/message.php');
} else {
    header("HTTP/1.1 403");
}
die;




