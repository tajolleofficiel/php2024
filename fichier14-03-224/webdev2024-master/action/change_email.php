<?php
// verifier si la 
var_dump($_POST);

// verification 
// si l'adresse mail est la meme que celle de la session
// SELECT email FROM user WHERE id=?
//



if(!empty($_SESSION['userid'])) {
    if (empty($_POST)) {
        header("HTTP/1.1 405");
        die;
    }
    $con = connect();
    // récupération de l'utilisateur correspondant à l'ID de session
    $result = $con->prepare("SELECT * FROM user WHERE id = ?");
    $result->execute([$_SESSION['userid']]);
    $user = $result->fetchObject();
    // vérification de l'existence de l'utilisateur
    if (!is_object($user)) {
        header("HTTP/1.1 401");
        die;
    }
    // l'email doit être valide
    $data = validDataType($_POST);
    // mise à jour de l'email
    $update = $con->prepare("UPDATE user SET email = ? WHERE id = ?");
    $update->execute([$data['email'], $user->id]);
    // vérification de la mise à jour
    if ($update->rowCount()) {
        $_SESSION['message'] = "L'email $user->email a été mise à jour en " . $data['email'];
    } else {
        $_SESSION['message'] = "L'email n'a pas été mise à jour";
    }
    $redirect = 'Location: index.php?slug=view/message.php';
} else {
    $redirect = "HTTP/1.1 401";
}
header($redirect);
die;
