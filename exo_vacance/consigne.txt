<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Script Serveur</title>
</head>
<body>
<!--
    1/ Inclure un menu de navigation HTML contenant des liens menant à différentes pages :
        - Page d'accueil (index.php)
        - Formulaire de création de compte (view/signup.html)
        - Formulaire de login (view/login.html)
    2/ Séparer le contenu de ce fichier en plusieurs parties à inclure dans index.php :
        - header.html (contenant le début du code HTML, jusqu'au <head> inclu)
        - menu.php (contenant le menu généré au point 1/ )
        - content.php (contenant le contenu principal de la page sélectionnée dans le menu)
        - footer.html (contenant le footer)
-->
<?php include_once 'view/login.html'; ?>
</body>
</html>
