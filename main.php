<?php
$array = [
    "email" => "test@test.com", // rtrim() a chaque valeur associer ? 
    "password" => "Password1",
    "ip" => "?",
    "time" => "?" // la valeur de time depuis 1970 jusq'ua mtn
];

// -- PREMIERE ETAPE --

// epace blanc fonction .trim(...)
// doit-on mettre dans le tableau directement ? 

function cleanData(string $data): string
{
    // CECI EST BON AUSSI, Il est juste decompose 
    // $data = trim($data);
    // $data = strip_tags($data);
    // return $data;
    return strip_tags(trim($data));
}

function validDataType(array $data): array {
    foreach ($data as $key => $value)
        ;
    if ($key == 'email') {
        $data['email'] = filter_var($value, FILTER_VALIDATE_EMAIL); //email ou $key
        // on utilise la fonction native "filter_var", avec la $valeur 
        // pour tester en deuxieme parametre FILTER... Constante, elle est ne majuscule
        // car c'est une constante.
        // 
    } elseif ($key == 'password') {
        if (
            strlen($value) < 8
            // strlen c'est string + lenght = str...len...., pour la longueur d'un

            || !preg_match("/[\d]/", $value) // de 0 a 9
            || !preg_match("/[a-z]/", $value)
            || !preg_match("/[A-Z]/", $value)
            || !preg_match("/[0-9]/", $value) // idem que \d
            || !preg_match("/[\W]/", $value) // caractere speciaux 
            // ! sert a faire une negation 
        ) {
            $data['password'] = false;
        }
    } elseif ($key == 'ip') {
        $data['ip'] = filter_var($value, FILTER_VALIDATE_IP);
    } elseif ($key == 'timestamp') {
        $date = new \DateTime();
        $date->setTimestamp($data['timestamp']);
        // "->" sert a utilier un methode  
        if ($date != new \DateTime()) {
            // date va etre de type objet + le type datetime
            // DateTime est mieux que le simple date, car il est utilisable pour calculer
            // pour etre comparer etc. que date faudra le convertir etc

            $data['timestanmp'] = false;
        }
    }
    return $data;
}


// :string pour pouvoir retourner une valeur string
// ne pas oublier le RETRUN car obligatoire !
// STRIP_TAGS et TRIM sont des functions natives de PHP 




// for tout les element du tableau, il faut verifier 
//  l'email doit etre valider, je ferais un boucle avec un "if"
// en verifiant si il y a un ..."@"..."."... / ca doit resembler a cA = ["%"."@"."%"."."."%"] ? 
// ctype_upper() = ? pour verifier si il y a bien une majuscule au minimum
// ctyper_lower() = ? pour voir si il y a au minimum 

// valeur de sortie, doit renvoyer un tableau

?>