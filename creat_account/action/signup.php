<?php

/**
* 1e étape :
*      Créez un script permettant de récupérer les données du formulaire de signup et affichant, 
*      dans un tableau HTML, chaque donnée avec son intitulé et sa valeur
*
*  2e étape :
*      Utilisez la fonction validDataType() sur chaque donnée envoyée par le formulaire.
*
*  3e étape :
*      Inclure la date d'inscription
*      Aborder la superglobale $_SERVER afin de récupérer l'adresse IP
*
*/

/*input*/
include_once'../../main.php';

$output ='';

/* Instruction  */

if(empty($_POST)){
    header("HTTP/1.1 405");
    die;
}

$data = validDataType($_POST); 
/*  traintement donc dans les instructions
    on l'affecte dans la variable $data
    conmme c'est un return, il ne revois pas un boleen, 
    on est obligé de mettre dans la variable
*/

    foreach($data as $key => $value){
        $output.='<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
    }
/* Output */
echo '<table>
        <tr>
            <th>Intitules</th>
            <th>Valeurs</th>
        </tr>
    '.$output.'
</table>'


?>
